#include "imagem.h"
#include "base.h"
#include "cores.h"
#include "geometria.h"
#include "desenho.h"
#include "segmenta.h"
#include "filtros2d.h"

#include <stdio.h>
#include <stdlib.h>

/*============================================================================*/

#define INPUT_IMAGE "GT2.BMP"
#define THRESHOLD 0.7f
#define ALPHA 0.95
#define BETA 0.099

/*#define INPUT_IMAGE "gc.bmp"
#define THRESHOLD 0.5f
#define ALPHA 0.99
#define BETA 0.03*/

/*============================================================================*/

void bright_pass(Imagem* in, Imagem* out, float threshold);

/*============================================================================*/

int main()
{
	int channel, row, col;
	/**Abertura da Imagem**/
	Imagem* img = abreImagem (INPUT_IMAGE, 3);
	if (!img)
	{
		printf ("Erro abrindo a imagem.\n");
		exit (1);
	}

	/*Cria imagem out e aux*/
	Imagem* out = criaImagem (img->largura, img->altura, 3);
	Imagem* aux1 = criaImagem (img->largura, img->altura, 3);
	Imagem* aux2 = criaImagem (img->largura, img->altura, 3);
	Imagem* aux3 = criaImagem (img->largura, img->altura, 3);

	/**USANDO BLUR**/
	/*Isola as fontes de luz*/
	bright_pass(img, out, THRESHOLD);

	//copiaConteudo (out, out);
	/*faz várias vezes o filtro da média*/
	blur (out, aux1, 9, 9, NULL);
	blur (aux1, aux2, 15, 15, NULL);
	blur (aux2, aux3, 19, 19, NULL);

	/*soma os resultados*/
    for (channel = 0; channel < img->n_canais; channel++)
        for (row = 0; row < img->altura; row++)
            for (col = 0; col < img->largura; col++)
            	out->dados[channel][row][col] = aux1->dados[channel][row][col] + aux2->dados[channel][row][col] + aux3->dados[channel][row][col];

    /*soma a imagem original com o resultado final*/
    for (channel = 0; channel < img->n_canais; channel++)
        for (row = 0; row < img->altura; row++)
            for (col = 0; col < img->largura; col++)
            	out->dados[channel][row][col] = img->dados[channel][row][col]*ALPHA + out->dados[channel][row][col]*BETA;
               	
	/*salva o resultado*/
	salvaImagem (out, "resultadoBlur.bmp");

	/**USANDO GAUSSIANO**/
	bright_pass(img, out, THRESHOLD);
	filtroGaussiano (out, aux1, 3, 3, NULL);
	filtroGaussiano (aux1, aux2, 5, 5, NULL);
	filtroGaussiano (aux2, aux3, 7, 7, NULL);
    for (channel = 0; channel < img->n_canais; channel++)
        for (row = 0; row < img->altura; row++)
            for (col = 0; col < img->largura; col++)
            	out->dados[channel][row][col] = aux1->dados[channel][row][col] + aux2->dados[channel][row][col] + aux3->dados[channel][row][col];
    for (channel = 0; channel < img->n_canais; channel++)
        for (row = 0; row < img->altura; row++)
            for (col = 0; col < img->largura; col++)
            	out->dados[channel][row][col] = img->dados[channel][row][col]*ALPHA + out->dados[channel][row][col]*BETA;

	/*salva o resultado*/
	salvaImagem (out, "resultadoGaussiano.bmp");

	/*libera memória*/
	destroiImagem(img);
	destroiImagem(out);
	return 0;
}
void bright_pass(Imagem* in, Imagem* out, float threshold)
{
    if (in->largura != out->largura || in->altura != out->altura || in->n_canais != out->n_canais)
    {
        printf ("ERRO: binariza: as imagens precisam ter o mesmo tamanho e numero de canais.\n");
        exit (1);
    }

    int channel, row, col;
    for (channel = 0; channel < in->n_canais; channel++)
        for (row = 0; row < in->altura; row++)
            for (col = 0; col < in->largura; col++)
                if (in->dados[channel][row][col] < threshold)
                	out->dados[channel][row][col] = 0;
                else
                	out->dados[channel][row][col] = in->dados[channel][row][col];
}
