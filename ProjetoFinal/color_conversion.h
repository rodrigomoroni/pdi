/*============================================================================*/
/* Conversão de LAB para RGB e vice-versa                                     */
/*----------------------------------------------------------------------------*/
/* Autor: Rodrigo G. Moroni de Souza - rodrigomoroni@hotmail.com              */
/*============================================================================*/

#include <math.h>

/*============================================================================*/

void RGBtoLAB(int red, int green, int blue, float *l, float *a, float *b);
void LABtoRGB(float l, float a, float b, int *red, int *green, int *blue);

/*============================================================================*/
