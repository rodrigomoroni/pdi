/*============================================================================*/
/* Conversão de LAB para RGB e vice-versa                                     */
/*----------------------------------------------------------------------------*/
/* Autor: Rodrigo G. Moroni de Souza - rodrigomoroni@hotmail.com              */
/*============================================================================*/

#include "color_conversion.h"

/*============================================================================*/

/**RGBtoLAB*/
/***********************************************************
RGB
R: 0 a 255
B: 0 a 255
G: 0 a 255
cieLAB
L: 0 a 100
A: -128 a 127
B: -128 a 127
Entrada: valor RGB, ponteiros LAB
Saída: valores resultantes serão colocados nos ponteiros LAB
************************************************************/
void RGBtoLAB(int red, int green, int blue, float *l, float *a, float *b)
{

    /**********************************************
    Primeiro é feita a conversão do RGB para o XYZ
    Em seguida do XYZ para o cieLAB
    **********************************************/

    int i;

    /*Em vetores pra ficar mais claro*/
    float RGB[3] = {red, green, blue};
    float XYZ[3] = {0, 0, 0};

    /**RGB para o XYZ*/
    /*Para cada valor no RGB vamos fazer uma continha*/
    for (i=0; i < 3; i++)
    {
        RGB[i] = (float)RGB[i]/255;

        if (RGB[i] > 0.04045)
            RGB[i] = pow(((RGB[i] + 0.055)/1.055), 2.4);
        else
            RGB[i] = RGB[i]/12.92;

        RGB[i] = RGB[i] * 100;
    }

    /*Multiplicação de matriz (padrão)*/
    XYZ[0] = RGB[0] * 0.4124 + RGB[1] * 0.3576 + RGB[2] * 0.1805;
    XYZ[1] = RGB[0] * 0.2126 + RGB[1] * 0.7152 + RGB[2] * 0.0722;
    XYZ[2] = RGB[0] * 0.0193 + RGB[1] * 0.1192 + RGB[2] * 0.9505;

    /*Padrão, agora está feita a conversão para o XYZ*/
    XYZ[0] = (float)XYZ[0]/95.047;
    XYZ[1] = (float)XYZ[1]/100.0;
    XYZ[2] = (float)XYZ[2]/108.883;

    /**Agora XYZ para o cieLAB*/
    /*Para cada valor no XYZ vamos fazer uma continha*/
    for (i=0; i < 3; i++)
    {
        if (XYZ[i] > 0.008856)
            XYZ[i] = pow(XYZ[i], 0.3333333333333333);
        else
            XYZ[i] = (7.787 * XYZ[i]) + (16/116);
    }

    /*Agora está feita a conversão para o cieLAB*/
    *l = (116 * XYZ[1]) - 16;
    *a = 500 * (XYZ[0] - XYZ[1]);
    *b = 200 * (XYZ[1] - XYZ[2]);
}

/**LABtoRGB*/
/***********************************************************
RGB
R: 0 a 255
B: 0 a 255
G: 0 a 255
cieLAB
L: 0 a 100
A: -128 a 127
B: -128 a 127
Entrada: valor LAB, ponteiros RGB
Saída: valores resultantes serão colocados nos ponteiros RGB
************************************************************/
void LABtoRGB(float l, float a, float b, int *red, int *green, int *blue)
{
    /**********************************************
    Primeiro é feita a conversão do LAB para o XYZ
    Em seguida do XYZ para o RGB, ou seja, as ope
    rações inversas do que fiz na função RGBtoLAB
    **********************************************/
    int i;

    /*Em vetores pra ficar mais claro*/
    float RGB[3] = {0, 0, 0};
    float XYZ[3] = {0, 0, 0};

    //LAB para o XYZ
    XYZ[1] = (l + 16)/116;
    XYZ[0] = a/500 + XYZ[1];
    XYZ[2] = XYZ[1] - b/200;

    //apenas o inverso do que fizemos, raiz(1/0,33333) do XYZ
    for (i=0; i < 3; i++)
    {
        if (pow(XYZ[i], 3) > 0.008856)
            XYZ[i] = pow(XYZ[i], 3);
        else
            XYZ[i] = (XYZ[i] - 16/116)/7.787;
    }

    //Antes dividíamos, agora inverso
    XYZ[0] = (float)XYZ[0]*95.047;
    XYZ[1] = (float)XYZ[1]*100.0;
    XYZ[2] = (float)XYZ[2]*108.883;

    //XYZ para o RGB
    XYZ[0] /= 100;
    XYZ[1] /= 100;
    XYZ[2] /= 100;

    //Multiplicação de matriz novamente, mas com outros fatores (padrão)
    RGB[0] = XYZ[0] * 3.240479  + XYZ[1] * -1.53715 + XYZ[2] * -0.498535;
    RGB[1] = XYZ[0] * -0.969256 + XYZ[1] * 1.875991  + XYZ[2] * 0.041556;
    RGB[2] = XYZ[0] * 0.055648 + XYZ[1] * -0.204043 + XYZ[2] * 1.057311;

    //apenas o inverso, mudando um pouco o fator
    for (i=0; i < 3; i++)
    {
        if (RGB[i] > 0.0031308)
            RGB[i] = pow(RGB[i], 1/2.4)*1.055 - 0.055;
        else
            RGB[i] = RGB[i]*12.92;
    }

    //está entre 0-1, multiplicamos por 255 para ficar entre 0-255
    *red = RGB[0] * 255;
    *green = RGB[1] * 255;
    *blue = RGB[2] * 255;
}
