#include "imagem.h"
#include "base.h"
#include "cores.h"
#include "geometria.h"
#include "desenho.h"
#include "segmenta.h"
#include "filtros2d.h"
#include "color_conversion.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*============================================================================*/

#define NUM_ARQUIVOS 14

//os angulos (radianos) para cada daltonismo estão no vetor de ANGULOS
float ANGULOS[] =
{
    -0.1415462, //deuteranopia -> ausência do verde
    -20.03638, //protanopia -> ausência do vermelho
    0.80930917 //tritanopia -> ausência do azul
};
char* ARQUIVOS [] =
{
    "img/azul01.bmp",
    "img/colorido01.bmp",
    "img/colorido02.bmp",
    "img/testeDicromatismo.bmp",
    "img/testeDicromatismo2.bmp",
    "img/testeDicromatismo3.bmp",
    "img/testeDicromatismo4.bmp",
    "img/testeDicromatismo5.bmp",
    "img/testeDicromatismo6.bmp",
    "img/verde01.bmp",
    "img/verde02.bmp",
    "img/vermelho01.bmp",
    "img/vermelho02.bmp",
    "img/vermelho03.bmp"
};

/*============================================================================*/
void imagem_RGB2LAB(Imagem *in, Imagem *out);
void imagem_LAB2RGB(Imagem *in, Imagem *out);
void projecao(Imagem *in, Imagem *out, float angulo);
/*============================================================================*/

int main()
{
    int i, j;
    char local[30]; //caminho onde será salva a imagem final
    Imagem *img, *aux, *out, *resultado;

    for (i = 0; i < NUM_ARQUIVOS; i++)
    {
        printf("Imagem: %d\n", i); //apenas para controle
        img=abreImagem(ARQUIVOS[i], 3); //obrigatoriamente deve ser aberta em 3 canais
        aux=criaImagem(img->largura, img->altura, 3); //crio imagem auxiliar
        out=criaImagem(img->largura, img->altura, 3);
        resultado=criaImagem(img->largura, img->altura, 3);

        //teste de abertura da imagem
        if (!img)
        {
            printf("Erro na abertura da imagem!\n \timagem: %d\n", i);
            return 0;
        }

        /*converte para o LAB, resultado no aux*/
        imagem_RGB2LAB(img, aux);

        //realiza o cáculo para cada Angulos possível
        for (j = 0; j < 3; j++)
        {
            /*Realiza os cálculos de projeção*/
            projecao(aux, out, ANGULOS[j]);

            /*volta para o RGB, resultado no resultado*/
            imagem_LAB2RGB(out, resultado);

            /*Salva resultado*/
            sprintf(local, "resultados/%d_%d.bmp", i, j);
            salvaImagem (resultado, local);
        }
    }
    destroiImagem(aux);
    destroiImagem(img);
    destroiImagem(out);
    destroiImagem(resultado);
	return 0;
}

/**imagem_RGB2LAB**/
/***********************************************
Para cada pixel da imagem, faz o cálculo da con
versão (chama RGBtoLAB) e seta na imagem out
Obviamente as imagens devem ter o mesmo tamanho
e três canais.
Entrada: imagem em RGB
Saida: Imagem em LAB (canal 0 -> L...)
***********************************************/
void imagem_RGB2LAB(Imagem *in, Imagem *out)
{
    int x, y;
    float l, a, b;
    for (x = 0; x < in->altura; x++)
    {
        for (y = 0; y < in->largura; y++)
        {
            RGBtoLAB(in->dados[0][x][y]*255, in->dados[1][x][y]*255, in->dados[2][x][y]*255, &l, &a, &b);
            out->dados[0][x][y] = l;
            out->dados[1][x][y] = a;
            out->dados[2][x][y] = b;
        }
    }
}

/**imagem_LAB2RGB**/
/***********************************************
Para cada pixel da imagem, faz o cálculo da con
versão (chama LABtoRGB) e seta na imagem out
Obviamente as imagens devem ter o mesmo tamanho
e três canais.
Entrada: imagem em LAB
Saida: Imagem em RGB
***********************************************/
void imagem_LAB2RGB(Imagem *in, Imagem *out)
{
    int x, y, r, g, b;
    for (x = 0; x < in->altura; x++)
    {
        for (y = 0; y < in->largura; y++)
        {
            LABtoRGB(in->dados[0][x][y], in->dados[1][x][y], in->dados[2][x][y], &r, &g, &b);
            out->dados[0][x][y] = (float)r/255;
            out->dados[1][x][y] = (float)g/255;
            out->dados[2][x][y] = (float)b/255;
        }
    }
}

/**projecao*/
/**********************************************
Para cada pixel da imagem, faz o cálculo da pro
jeção do pixel na reta, com angulo da reta, re
resultado colocado na imagem out.
Entrada: imagem em LAB
Saída: imagem em LAB, com projeção dos pixels
**********************************************/
void projecao(Imagem *in, Imagem *out, float angulo)
{
    float t, alfa, lambda, seno, cosseno;
    int x, y;

    //faz o cáculo antes para evitar repetidas chamadas da função
    seno = sin(angulo);
    cosseno = cos(angulo);

    //para cada pixel da imagem em LAB
    for (x = 0; x < in->altura; x++)
    {
        for (y = 0; y < in->largura; y++)
        {
            t = sqrt(pow(in->dados[1][x][y], 2) + pow(in->dados[2][x][y], 2));
            alfa = atan2(in->dados[1][x][y], in->dados[2][x][y]);
            lambda = t*cos(fabs(angulo - alfa));

            out->dados[1][x][y] = lambda*seno; //projeção da reta
            out->dados[2][x][y] = lambda*cosseno;
            out->dados[0][x][y] = in->dados[0][x][y]; //L é ignorado no cálculo, apenas copia o valor.
        }
    }
}
