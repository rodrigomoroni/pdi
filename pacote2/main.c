/*============================================================================*/
/* Exemplo: segmenta��o de uma imagem em escala de cinza.                     */
/*----------------------------------------------------------------------------*/
/* Autor: Bogdan T. Nassu                                                     */
/* Universidade Tecnol�gica Federal do Paran�                                 */
/*============================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "base.h"
#include "cores.h"
#include "desenho.h"
#include "geometria.h"
#include "imagem.h"
/*============================================================================*/

#define INPUT_IMAGE "arroz.bmp"

#define NEGATIVO 0
#define THRESHOLD 0.8f
#define ALTURA_MIN 10
#define LARGURA_MIN 10
#define N_PIXELS_MIN 80
#define TAM_COMPONENTES 30 //tamanho inicial do vetor de componentes

/*============================================================================*/

typedef struct
{
    float label;
    Retangulo roi;
    int n_pixels;

} Componente;

/*============================================================================*/

void binariza (Imagem* in, Imagem* out, float threshold);
int rotula (Imagem* img, Componente** componentes, int largura_min, int altura_min, int n_pixels_min);

/*============================================================================*/

int main ()
{
    int i;

    // Abre a imagem em escala de cinza, e mant�m uma c�pia colorida dela para desenhar a sa�da.
    Imagem* img = abreImagem (INPUT_IMAGE, 1);
    if (!img)
    {
        printf ("Erro abrindo a imagem.\n");
        exit (1);
    }

    Imagem* img_out = criaImagem (img->largura, img->altura, 3);
    cinzaParaRGB (img, img_out);

    // Segmenta a imagem.
    if (NEGATIVO)
    inverte (img, img);
    binariza (img, img, THRESHOLD);
    salvaImagem (img, "01 - binarizada.bmp");

    Componente* componentes;
    int n_componentes;
    clock_t tempo_inicio = clock ();
    n_componentes = rotula (img, &componentes, LARGURA_MIN, ALTURA_MIN, N_PIXELS_MIN);
    clock_t tempo_total = clock () - tempo_inicio;

    printf ("Tempo: %d\n", (int) tempo_total);
    printf ("%d componentes detectados.\n", n_componentes);

    // Mostra os objetos encontrados.

    for (i = 0; i < n_componentes; i++)
    desenhaRetangulo (componentes [i].roi, criaCor (1,0,0), img_out);
    salvaImagem (img_out, "02 - out.bmp");

    // Limpeza.
    free (componentes);
    destroiImagem (img_out);
    destroiImagem (img);
    return (0);
}
/*----------------------------------------------------------------------------*/
/** Binariza��o simples por limiariza��o.
*
* Par�metros: Imagem* in: imagem de entrada. Se tiver mais que 1 canal,
*               binariza cada canal independentemente.
*             Imagem* out: imagem de sa�da. Deve ter o mesmo tamanho da
*               imagem de entrada.
*             float threshold: limiar.
*
* Valor de retorno: nenhum (usa a imagem de sa�da). */

void binariza (Imagem* in, Imagem* out, float threshold)
{
    if (in->largura != out->largura || in->altura != out->altura || in->n_canais != out->n_canais)
    {
        printf ("ERRO: binariza: as imagens precisam ter o mesmo tamanho e numero de canais.\n");
        exit (1);
    }

    int i, j, k;
    for (i = 0; i < in->altura; i++)
    {
        for (j = 0; j < in->largura; j++)
        {
            for (k = 0; k < in->n_canais; k++)
            {
                if (in->dados[k][i][j] > THRESHOLD)
                {
                    out->dados[k][i][j] = 255;
                }
                else
                {
                    out->dados[k][i][j] = 0;
                }
            }
        }
    }
}
/*============================================================================*/
/* ROTULAGEM                                                                  */
/*============================================================================*/
/** Rotulagem usando flood fill. Marca os objetos da imagem com os valores
* [0.1,0.2,etc].
*
* Par�metros: Imagem* img: imagem de entrada E sa�da.
*             Componente** componentes: um ponteiro para um vetor de sa�da.
*               Supomos que o ponteiro inicialmente � inv�lido. Ele ir�
*               apontar para um vetor que ser� alocado dentro desta fun��o.
*               Lembre-se de desalocar o vetor criado!
*             int largura_min: descarta componentes com largura menor que esta.
*             int altura_min: descarta componentes com altura menor que esta.
*             int n_pixels_min: descarta componentes com menos pixels que isso.
*
* Valor de retorno: o n�mero de componentes conexos encontrados. */
void flood(int tam_label, Componente** componentes, Imagem *img, int x, int y)
{
    img->dados[0][x][y] = tam_label;

    //verifica para cada vizinho
    if (((x-1) >= 0) && img->dados[0][x-1][y] == 255)
    {
        (*componentes)[tam_label-1].n_pixels++;
        flood(tam_label, componentes, img, x-1, y);
    }
    if ((x+1) <= img->altura && img->dados[0][x+1][y] == 255) //se a posição é válida
    {
        (*componentes)[tam_label-1].n_pixels++;
        flood(tam_label, componentes, img, x+1, y);

        if ((x+2) <= img->altura && img->dados[0][x+2][y] == 0
        && (x+1) >= (*componentes)[tam_label-1].roi.b)
        {
            (*componentes)[tam_label-1].roi.b = x+1;
        }
    }
    if ((y-1) >= 0 && img->dados[0][x][y-1] == 255)
    {
        (*componentes)[tam_label-1].n_pixels++;
        flood(tam_label, componentes, img, x, y-1);

        if ((y-2) >= 0 && img->dados[0][x][y-2] == 0
        && (y-1) <= (*componentes)[tam_label-1].roi.e)
        {
            (*componentes)[tam_label-1].roi.e = y-1;
        }
    }
    if ((y+1) <= img->largura && img->dados[0][x][y+1] == 255)
    {
        (*componentes)[tam_label-1].n_pixels++;
        flood(tam_label, componentes, img, x, y+1);

        if ((y+2) <= img->largura && img->dados[0][x][y+2] == 0
        && (y+1) >= (*componentes)[tam_label-1].roi.d)
        {
            (*componentes)[tam_label-1].roi.d = y+1;
        }
    }
}
int reduz_ruido(int tam_label, Imagem* img, Componente** componentes, int largura_min, int altura_min, int n_pixels_min)
{
    int i = 0;
    while (i < tam_label) //percorre todo vetor de componentes
    {
        if ((*componentes)[i].n_pixels < n_pixels_min ||
        ((*componentes)[i].roi.b - (*componentes)[i].roi.c) < altura_min ||
        ((*componentes)[i].roi.d - (*componentes)[i].roi.e) < largura_min) //parametros minimos para ser um arroz
        {
            //retira do vetor os não-arroz
            int j;
            for (j=i; j < tam_label - 1; j++)
            {
                (*componentes)[j] = (*componentes)[j + 1];
            }
            tam_label--; //decremento o tamanho do vetor (retirei um elemento)
            (*componentes) = realloc((*componentes), sizeof(Componente) * tam_label); //faço realloc pra reduzir o vetor
        }
        else
        {
            i++; //se não houve reposicionamento no vetor, incrementa
        }
    }
    return tam_label;
}
int rotula (Imagem* img, Componente** componentes, int largura_min, int altura_min, int n_pixels_min)
{
    int tam = TAM_COMPONENTES;
    int tam_label = 1;
    int i, j;
    *componentes = malloc(sizeof(Componente) * tam); //alocação dos componentes

    for (i = 0; i < img->altura; i++) //para cada pixel
    {
        for (j = 0; j < img->largura; j++) //da imagem
        {
            if (img->dados[0][i][j] == 255) //se for branco (objeto)
            {
                (*componentes)[tam_label-1].label = (float)tam_label/10; //label do componente
                (*componentes)[tam_label-1].n_pixels = 1;

                (*componentes)[tam_label-1].roi.c = i; //primeiro pixel sempre é 'cima'
                (*componentes)[tam_label-1].roi.b = i; //inicializa os valores do retangulo para comparação...
                (*componentes)[tam_label-1].roi.e = j;
                (*componentes)[tam_label-1].roi.d = j;

                flood(tam_label, componentes, img, i, j); //inunda

                tam_label++; //incrementa tamanho do vetor

                if (tam_label > tam) //verifico se passei do tamanho do vetor
                {
                    tam++;
                    *componentes = realloc(*componentes, sizeof(Componente) * tam); //faço realloc se necessário
                }
            }
        }
    }
    //printf("%f, %d\n", (*componentes)[tam_label-1].label, (*componentes)[tam_label-1].n_pixels);
    tam_label--;
    tam_label = reduz_ruido(tam_label, img, componentes, largura_min, altura_min, n_pixels_min);
    return tam_label;
}



/*============================================================================*/
