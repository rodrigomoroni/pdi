#include "imagem.h"
#include "base.h"
#include "cores.h"
#include "geometria.h"
#include "desenho.h"
#include "segmenta.h"

#include <stdio.h>
#include <stdlib.h>

/*============================================================================*/

#define INPUT_IMAGE "Exemplos/b01 - Original.bmp"

/*============================================================================*/

void filtroMediaSimples(Imagem *img, Imagem *out, int lado, int altura);
void filtroMediaSeparavel(Imagem *img, Imagem *out, int lado, int altura);
void filtroMediaIntegral(Imagem *img, Imagem *out, int lado, int altura);

/*============================================================================*/

int main()
{
	/**Abertura da Imagem**/
	Imagem* img = abreImagem (INPUT_IMAGE, 3);
	if (!img)
	{
		printf ("Erro abrindo a imagem.\n");
		exit (1);
	}

	/**Cria Imagem Out e copia conteudo da img original**/
	Imagem* out = criaImagem (img->largura, img->altura, 3);
	//copiaConteudo (img, out);

	/**Chama Filtro Média Simples**/
	filtroMediaSimples(img, out, 7, 7);
	/**Salva Imagem Media Simples**/
	salvaImagem (out, "01 - outSimples.bmp");

	/**Chama Fitro Media Separavel**/
	//filtroMediaSeparavel(img, out, 7, 7);
	/**Salva Imagem Media Separavel**/
	salvaImagem (out, "01 - outSeparavel.bmp");

	/**Chama Filtro Media Integral**/
	filtroMediaIntegral(img, out, 7, 7);
	destroiImagem(img);
	/**Salva Imagem Media Integral**/
	salvaImagem (out, "01 - outIntegral.bmp");

	/**Destroi imagens**/
	destroiImagem(out);
	return 0;
}

/**Filtro Média Simples**/
void filtroMediaSimples(Imagem *img, Imagem *out, int lado, int altura)
{
	if (img->largura != out->largura || img->altura != out->altura || img->n_canais != out->n_canais)
	{
		printf ("ERRO: binariza: as imagens precisam ter o mesmo tamanho e numero de canais.\n");
		exit (1);
	}

	int i, j, k, auxi, auxj, count;
	lado = (lado-1)/2;
	altura = (altura-1)/2;
	float soma;
	for (i = 0; i < img->altura; i++) //para cada
	{
		for (j = 0; j < img->largura; j++) //pixel na imagem
		{
			for (k = 0; k < img->n_canais; k++) //para cada canal
			{
				if (i < altura || j < lado || i + 1 > img->altura - altura || j + 1 > img->largura - lado) //se não tiver 'janela'
				{
					out->dados[k][i][j] = img->dados[k][i][j]; //ignora as margens
				}
				else //se tiver 'janela'
				{
					soma = 0.0;
					count = 0;
					for (auxi = i - altura; auxi <= i + altura; auxi++) //para cada
					{
						for (auxj = j - lado; auxj <= j +lado; auxj++) //vizinho do pixel
						{
							soma += img->dados[k][auxi][auxj];
							count++;
						}
					}
					out->dados[k][i][j] = (float)soma/count; //faz a média
				}
			}
		}
	}
}
/**Filtro Média Separavel**/
void filtroMediaSeparavel(Imagem *img, Imagem *out, int lado, int altura)
{
	if (img->largura != out->largura || img->altura != out->altura || img->n_canais != out->n_canais)
	{
		printf ("ERRO: binariza: as imagens precisam ter o mesmo tamanho e numero de canais.\n");
		exit (1);
	}

	int i, j, k, auxi, auxj, count;
	lado = (lado-1)/2;
	altura = (altura-1)/2;
	float soma, soma_aux[img->n_canais], subtracao, soma_coluna;
	Imagem *aux = criaImagem (img->largura, img->altura, 3);
	/*faz a média de cada linha*/
	for (i=0; i < img->altura; i++)
	{
		for (j=0; j < img->largura; j++)
		{
			for (k=0; k < img->n_canais; k++)
			{
				if (i < altura || j < lado || i + 1 > img->altura - altura || j + 1 > img->largura - lado) //se não tiver 'janela'
				{
					aux->dados[k][i][j] = 0;
				}
				else
				{
					for (auxj = j - lado; auxj <= j + lado; auxj++)
						aux->dados[k][i][j] += img->dados[k][i][auxj];
					aux->dados[k][i][j] /= (lado*2)+1;
				}
			}
		}
	}
	for (i=0; i < img->altura; i++)
	{
		for (j=0; j < img->largura; j++)
		{
			for (k=0; k < img->n_canais; k++)
			{
				if (i < altura || j < lado || i + 1 > img->altura - altura || j + 1 > img->largura - lado) //se não tiver 'janela'
				{
					out->dados[k][i][j] = img->dados[k][i][j];
				}
				else
				{
					for (auxi = i - altura; auxi <= i + altura; auxi++)
						out->dados[k][i][j] += img->dados[k][auxi][j];
					aux->dados[k][i][j] /= (altura*2)+1;
				}
			}
		}
	}
	destroiImagem(aux);
}
/**Filtro Média Integral**/
void filtroMediaIntegral(Imagem *img, Imagem *out, int lado, int altura)
{
	if (img->largura != out->largura || img->altura != out->altura || img->n_canais != out->n_canais)
	{
		printf ("ERRO: binariza: as imagens precisam ter o mesmo tamanho e numero de canais.\n");
		exit (1);
	}

	int i, j, k, count, aux_a, aux_l;
	int denominador = lado*altura;
	lado = (lado-1)/2;
	altura = (altura-1)/2;
	Imagem *aux = criaImagem (img->largura, img->altura, 3);

	/*Efetua a soma na 'imagem' aux*/
	for (i = 0; i < img->altura; i++) //para cada
	{
		for (k = 0; k < img->n_canais; k++)
		{
			aux->dados[k][i][0] = img->dados[k][i][0];
			for (j = 1; j < img->largura; j++)
			{
				aux->dados[k][i][j] = img->dados[k][i][j] + aux->dados[k][i][j-1];
			}
		}
	}

	for (i = 1; i < aux->altura; i++)
	{
		for (j = 0; j < aux->largura; j++)
		{
			for (k = 0; k < aux->n_canais; k++)
			{
				aux->dados[k][i][j] = aux->dados[k][i][j] + aux->dados[k][i-1][j];
			}
		}
	}

	/*Calcula Valor da Média*/
	for (i = 0; i < img->altura; i++) //para cada
	{
		for (j = 0; j < img->largura; j++) //pixel na imagem
		{
			for (k = 0; k < img->n_canais; k++) //para cada canal
			{
				if (i < altura + 1 || j < lado + 1 || i + 1 > img->altura - altura || j + 1 > img->largura - lado) //se não tiver 'janela'
				{
					aux_a = altura;
					aux_l = lado;
					if (i < aux_a + 1)
						aux_a = i;
					if (i + 1 > img->altura - aux_a)
						aux_a = img->altura - i - 1;
					if (j < aux_l + 1)
						aux_l = j;
					if (j + 1 > img->largura - aux_l)
						aux_l = img->largura - j - 1;

					//printf("%d, %d, %d, %d, %d, %d\n", i, j, aux_a, aux_l, altura, lado);
					out->dados[k][i][j] = (float)(aux->dados[k][i+aux_a][j+aux_l] - aux->dados[k][i-aux_a][j+aux_l] + aux->dados[k][i-aux_a][j-aux_l] - aux->dados[k][i+aux_a][j-aux_l])/(((aux_a*2)+1)*((aux_l*2)+1)); //faz a média
				}
				else //se tiver 'janela'
				{
					out->dados[k][i][j] = (float)(aux->dados[k][i+altura][j+lado] - aux->dados[k][i-altura-1][j+lado] + aux->dados[k][i-altura-1][j-lado-1] - aux->dados[k][i+altura][j-lado-1])/denominador; //faz a média
				}
			}
		}
	}
	destroiImagem(aux);
}
