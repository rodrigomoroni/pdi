#include "imagem.h"
#include "base.h"
#include "cores.h"
#include "geometria.h"
#include "desenho.h"
#include "segmenta.h"
#include "filtros2d.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*============================================================================*/
#define MAX_R 0.6f
#define MIN_G 0.5f
#define MAX_B 0.6f
#define LIM_H_SUP 150.0f
#define LIM_H_INF 85.0f
#define LIM_S_INF 0.2f
#define LIM_L_SUP 0.9f
#define LIM_L_INF 0.15f
#define NUM_ARQUIVOS 10
#define IMG "img/nz.bmp"
char* ARQUIVOS [] =
{
    "img/0.BMP",
    "img/1.bmp",
    "img/2.bmp",
    "img/3.bmp",
    "img/4.bmp",
	"img/5.bmp",
	"img/6.bmp",
	"img/7.bmp",
	"img/8.bmp",
    "img/9.bmp"
};

/*============================================================================*/
void chroma(Imagem *fundo, Imagem *frente);
void chromaRGB(Imagem *fundo, Imagem *frente);
void chromaFloodFill(Imagem *fundo, Imagem *frente);
void flood(Imagem *img, int x, int y, float desvioH, float desvioS, float desvioL);
void tratamento(Imagem *img, Imagem *medium, Imagem *out);
void desvioPadrao(Imagem *img, float *h, float *s, float *l, float mediaH, float mediaS, float mediaL);
void media(Imagem *img, float *h, float *s, float *l);
void mediaRange(Imagem *img, float *h, float *s, float *l);
void desvioPadraoRange(Imagem *img, float *h, float *s, float *l, float mediaH, float mediaS, float mediaL);
/*============================================================================*/
int main()
{
	int i;
	char local[20];
	Imagem *img, *out, *aux, *fundo, *aux_fundo;

    /**Abre imagem do fundo*/
    fundo = abreImagem(IMG, 3);

    /**Percorre cada uma das imagens*/
	for (i=0; i<NUM_ARQUIVOS; i++)
	{
        /**Abre a Imagem*/
		img=abreImagem(ARQUIVOS[i], 3);
		if (!img)
		{
            printf ("Nao conseguiu abrir %s\n", ARQUIVOS [i]);
            return (1);
        }

        /**Cria as Imagens Auxiliares*/
        out=criaImagem (img->largura, img->altura, 3);
		aux=criaImagem (img->largura, img->altura, 3);
        aux_fundo=criaImagem (img->largura, img->altura, 3);

        /**Redimensionamento*/
        redimensionaNN (fundo, aux_fundo);

		/**RGB para HSL*/
        RGBParaHSL (img, out);
        RGBParaHSL (aux_fundo, aux); //aqui as duas estão do mesmo tamanho, então posso converter

        /**Operações*/
        printf("%d: ", i);
        chroma(aux, out); //o que está em aux é a versão HSL do fundo...

		/**HSL para RGB*/
        copiaConteudo(out, aux); //passo o valor de out para aux
		HSLParaRGB(aux, out); //faço a conversão para RGB e está pronto para ser salvo
        HSLParaRGB(aux, aux_fundo);

        /**tratamento final*/
        tratamento(aux, aux_fundo, out);

        /**Salva o Resultado*/
		sprintf(local, "final/%d.bmp", i);
		salvaImagem (out, local);
	}

    destroiImagem(img);
    destroiImagem(out);
	destroiImagem(aux);
    destroiImagem(aux_fundo);
    destroiImagem(fundo);
	return 0;
}
void tratamento(Imagem *img, Imagem *medium, Imagem *out)
{
    int lado=9, altura=9, lin, col, can, auxcol, auxlin, count;
    float soma;
    lado = (lado-1)/2;
    altura = (altura-1)/2;
    for (can=0; can < 3; can++)
    {
        for (lin=0; lin < img->altura; lin++)
        {
            for (col=0; col < img->largura; col++)
            {
                if (lin < altura || col < lado || lin + 1 > img->altura - altura || col + 1 > img->largura - lado)
                    continue;
                if (
                    img->dados[0][lin][col] >= LIM_H_INF - 10
                    && img->dados[0][lin][col] <= LIM_H_SUP + 10
                    && img->dados[1][lin][col] >= LIM_S_INF
                    && img->dados[2][lin][col] >= LIM_L_INF - 0.05
                    && img->dados[2][lin][col] <= LIM_L_SUP
                )
                {
                    soma = 0.0;
                    count = 0;
                    for (auxlin = lin - altura; auxlin <= lin + altura; auxlin++)
                    {
                        for (auxcol = col - lado; auxcol <= col + lado; auxcol++)
                        {
                            soma += medium->dados[can][auxlin][auxcol];
                            count++;
                        }
                    }
                    out->dados[can][lin][col]=(float)soma/count; //faz a média
                }
            }
        }
    }
}
void chromaRGB(Imagem *fundo, Imagem *frente)
{
    Imagem *aux;
    aux = criaImagem(frente->largura, frente->altura, 3);

    copiaConteudo(frente, aux);
    HSLParaRGB(aux, frente);
    copiaConteudo(fundo, aux);
    HSLParaRGB(aux, fundo);

    int lin, col;
    for (lin=0; lin < fundo->altura; lin++)
	{
		for (col=0; col < fundo->largura; col++)
		{
            if (frente->dados[0][lin][col] <= MAX_R && frente->dados[1][lin][col] >= MIN_G
                && frente->dados[2][lin][col] <= MAX_B)
            {
                frente->dados[0][lin][col] = fundo->dados[0][lin][col];
                frente->dados[1][lin][col] = fundo->dados[1][lin][col];
                frente->dados[2][lin][col] = fundo->dados[2][lin][col];
            }
        }
    }

    copiaConteudo(frente, aux);
    RGBParaHSL(aux, frente);
}
void chroma(Imagem *fundo, Imagem *frente)
{
	int lin, col;
	float mediaH, mediaS, mediaL, desvioH, desvioS, desvioL;
    mediaRange(frente, &mediaH, &mediaS, &mediaL);
	desvioPadraoRange(frente, &desvioH, &desvioS, &desvioL, mediaH, mediaS, mediaL);

    desvioH*=-1;
    if (mediaS >= 0.9)
    {
        desvioS*=-1;
        desvioL*=-1;
        desvioL+=-desvioL*1/200;
    }
    else if(mediaS >= 0.8)
    {
        desvioS*=-1;
        desvioL*=-6;
    }
    else
    {
        desvioH*=0;
        desvioS*=0;
        desvioL=0;
    }

	for (lin=0; lin < fundo->altura; lin++)
	{
		for (col=0; col < fundo->largura; col++)
		{
			if (
                frente->dados[0][lin][col] >= LIM_H_INF + desvioH
                && frente->dados[0][lin][col] <= LIM_H_SUP
				&& frente->dados[1][lin][col] >= LIM_S_INF + desvioS
				&& frente->dados[2][lin][col] >= LIM_L_INF
                && frente->dados[2][lin][col] <= LIM_L_SUP + desvioL
                )
			{
                frente->dados[0][lin][col] = fundo->dados[0][lin][col];
                frente->dados[1][lin][col] = fundo->dados[1][lin][col];
                frente->dados[2][lin][col] = fundo->dados[2][lin][col];
			}
		}
	}

	printf("Média: %f, %f, %f\n", mediaH, mediaS, mediaL);
	//printf("Desvio: %f, %f, %f\n", desvioH, desvioS, desvioL);
}
void flood(Imagem *img, int x, int y, float desvioH, float desvioS, float desvioL)
{
    img->dados[0][x][y] = -1;
    printf("teste01\n");
    //verifica para cada vizinho
    if (((x-1) >= 0) && fabs(img->dados[0][x-1][y]-img->dados[0][x][y]) <= fabs(desvioH))
    {
        flood(img, x-1, y, desvioH, desvioS, desvioL);
    }
    printf("teste02\n");
    if (((x+1) <= img->altura) && fabs(img->dados[0][x+1][y]-img->dados[0][x][y]) <= fabs(desvioH)) //se a posição é válida
    {
        flood(img, x+1, y, desvioH, desvioS, desvioL);
    }
    printf("teste03\n");
    if (((y-1) >= 0) && fabs(img->dados[0][x][y-1]-img->dados[0][x][y]) <= fabs(desvioH))
    {
        flood(img, x, y-1, desvioH, desvioS, desvioL);
    }
    printf("teste04\n");
    if (((y+1) <= img->largura) && fabs(img->dados[0][x][y+1]-img->dados[0][x][y]) <= fabs(desvioH))
    {
        flood(img, x, y+1, desvioH, desvioS, desvioL);
    }
    printf("teste05\n");
}
void chromaFloodFill(Imagem *fundo, Imagem *frente)
{
	int lin, col;
	float mediaH, mediaS, mediaL, desvioH, desvioS, desvioL;
    mediaRange(frente, &mediaH, &mediaS, &mediaL);
	desvioPadraoRange(frente, &desvioH, &desvioS, &desvioL, mediaH, mediaS, mediaL);

    desvioH*=-1;
    if (mediaS >= 0.9)
    {
        desvioS*=-1;
        desvioL*=-1;
        desvioL+=-desvioL*1/200;
    }
    else if(mediaS >= 0.8)
    {
        desvioS*=-1;
        desvioL*=-6;
    }
    else
    {
        desvioH*=0;
        desvioS*=0;
        desvioL=0;
    }

	for (lin=0; lin < fundo->altura; lin++)
	{
		for (col=0; col < fundo->largura; col++)
		{
			if (
                frente->dados[0][lin][col] >= LIM_H_INF + desvioH
                && frente->dados[0][lin][col] <= LIM_H_SUP
				&& frente->dados[1][lin][col] >= LIM_S_INF + desvioS
				&& frente->dados[2][lin][col] >= LIM_L_INF
                && frente->dados[2][lin][col] <= LIM_L_SUP + desvioL
                )
			{
                flood(frente, lin, col, desvioH, desvioS, desvioL);
			}
		}
	}
}
void desvioPadrao(Imagem *img, float *h, float *s, float *l, float mediaH, float mediaS, float mediaL)
{
	int lin, col, tam;
	tam = img->altura*img->largura;
	*h=0;
	*s=0;
	*l=0;
	for (lin=0; lin < img->altura; lin++)
	{
		for (col=0; col < img->largura; col++)
		{
			*h += pow(img->dados[0][lin][col]-mediaH, 2);
			*s += pow(img->dados[1][lin][col]-mediaS, 2);
			*l += pow(img->dados[2][lin][col]-mediaL, 2);
		}
	}
	*h = sqrt(*h/tam);
	*s = sqrt(*s/tam);
	*l = sqrt(*l/tam);
}
void media(Imagem *img, float *h, float *s, float *l)
{
	int lin, col, tam;
	tam = img->altura*img->largura;
	*h=0;
	*s=0;
	*l=0;
	for (lin=0; lin < img->altura; lin++)
	{
		for (col=0; col < img->largura; col++)
		{
			*h += img->dados[0][lin][col];
			*s += img->dados[1][lin][col];
			*l += img->dados[2][lin][col];
		}
	}
	*h /= tam;
	*s /= tam;
	*l /= tam;
}
void desvioPadraoRange(Imagem *img, float *h, float *s, float *l, float mediaH, float mediaS, float mediaL)
{
	int lin, col, tam=0;
	*h=0;
	*s=0;
	*l=0;
	for (lin=0; lin < img->altura; lin++)
	{
		for (col=0; col < img->largura; col++)
		{
            if (
                img->dados[0][lin][col] >= LIM_H_INF
                && img->dados[0][lin][col] <= LIM_H_SUP
                && img->dados[1][lin][col] >= LIM_S_INF
                && img->dados[2][lin][col] >= LIM_L_INF
                && img->dados[2][lin][col] <= LIM_L_SUP
                )
            {
                *h += pow(img->dados[0][lin][col]-mediaH, 2);
                *s += pow(img->dados[1][lin][col]-mediaS, 2);
                *l += pow(img->dados[2][lin][col]-mediaL, 2);
                tam++;
            }
		}
	}
	*h = sqrt(*h/tam);
	*s = sqrt(*s/tam);
	*l = sqrt(*l/tam);
}
void mediaRange(Imagem *img, float *h, float *s, float *l)
{
    int lin, col, tam=0;
    *h=0;
    *s=0;
    *l=0;
    for (lin=0; lin < img->altura; lin++)
    {
        for (col=0; col < img->largura; col++)
        {
            if (
                img->dados[0][lin][col] >= LIM_H_INF
                && img->dados[0][lin][col] <= LIM_H_SUP
                && img->dados[1][lin][col] >= LIM_S_INF
                && img->dados[2][lin][col] >= LIM_L_INF
                && img->dados[2][lin][col] <= LIM_L_SUP
                )
            {
                *h += img->dados[0][lin][col];
                *s += img->dados[1][lin][col];
                *l += img->dados[2][lin][col];
                tam++;
            }
        }
    }
    *h /= tam;
    *s /= tam;
    *l /= tam;
}
