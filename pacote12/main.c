#include "imagem.h"
#include "base.h"
#include "cores.h"
#include "geometria.h"
#include "desenho.h"
#include "segmenta.h"
#include "filtros2d.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*============================================================================*/

#define THRESHOLD 0.1f
#define NUM_ARQUIVOS 5
#define ALTURA_MIN 5
#define LARGURA_MIN 5
#define N_PIXELS_MIN 20

char* ARQUIVOS [] =
{
    "60.bmp",
    "82.bmp",
    "114.bmp",
    "150.bmp",
    "205.bmp",
};

/*============================================================================*/

int contaArroz(ComponenteConexo **componentes, int tam);

/*============================================================================*/

int main()
{
	int i, n_componentes;
	char local[20];
	Imagem *img, *out, *kernel, *aux;
	ComponenteConexo *componentes;

    /**Ajusta o Kernel*/
	kernel = criaKernelCircular (7);
	Coordenada centro;
	centro.x = 3;
	centro.y = 3;

    /**Percorre cada uma das imagens*/
	for (i=0; i<NUM_ARQUIVOS; i++)
	{
        /**Abre a Imagem*/
		img=abreImagem(ARQUIVOS[i], 1);
		if (!img)
		{
            printf ("Nao conseguiu abrir %s\n", ARQUIVOS [i]);
            return (1);
        }

        /**Cria as Imagens Auxiliares*/
        out=criaImagem (img->largura, img->altura, 1);
		aux=criaImagem (img->largura, img->altura, 1);

		/**Aplica os filtros*/
        binarizaAdapt (img, out, 25, THRESHOLD, NULL);
		copiaConteudo(out, aux);
		abertura (aux, kernel, centro, out, NULL);

		/**Rotulaçao*/
		copiaConteudo(out, aux);
		n_componentes = rotulaFloodFill (aux, &componentes, LARGURA_MIN, ALTURA_MIN, N_PIXELS_MIN);
        printf("%d\n", contaArroz(&componentes, n_componentes));

        /**Salva o Resultado*/
		sprintf(local, "Resultados/%s", ARQUIVOS[i]);
		salvaImagem (out, local);
	}

    destroiImagem(img);
    destroiImagem(out);
	destroiImagem(kernel);
	destroiImagem(aux);
    free(componentes);
	return 0;
}

int contaArroz(ComponenteConexo **componentes, int tam)
{
    int i, soma=0, count=tam;
    for (i=0; i<tam; i++)
        soma+=(*componentes)[i].n_pixels;
    soma/=tam;

    for (i=0; i<tam; i++)
    {
        if ((*componentes)[i].n_pixels >= (soma + soma*2/3))
            count+=2;
        else if ((*componentes)[i].n_pixels >= (soma + soma/3 + soma/16))
            count++;
    }
    return count;
}
